#include <iostream>


void PrintOddorEvenNums(int N)
{
	for (int num = N % 2; num <= N; num += 2)
	{
		std::cout << num << '\n';
	}
}

int main()
{
	PrintOddorEvenNums(11);
}